package json.creator;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class FeatureTimeTuple implements Comparable<FeatureTimeTuple>{
    private int inputParam;
    private int measuredTime;

    public FeatureTimeTuple(Integer param, Integer measure) {
        inputParam = param;
        measuredTime = measure;
    }

    public int getInputParam() {
        return inputParam;
    }

    public int getMeasuredTime() {
        // don't remove. it's used by jackson when converting Samples list to json
        return measuredTime;
    }

    @Override
    public int compareTo(FeatureTimeTuple otherSample) {
        return Integer.compare(inputParam, otherSample.getInputParam());
    }
}
