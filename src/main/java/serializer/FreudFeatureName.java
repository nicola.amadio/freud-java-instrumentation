package serializer;

public class FreudFeatureName {
    private short varNameLen;
    private String varName;

    public FreudFeatureName(short varNameLen, String varName) {
        this.varNameLen = varNameLen;
        this.varName = varName;
    }

    public short getVarNameLen() {
        return varNameLen;
    }

    public String getVarName() {
        return varName;
    }
}
