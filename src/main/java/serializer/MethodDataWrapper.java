package serializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MethodDataWrapper {
    String instrumentationDir = "/home/nicolaamadio/Documents/tesi/freud-java-instrumentation/";

    // FILE NAME
    private int symbolNameLen;
    private String symbolName;
    // FEATURE NAMES
    private int featureNamesCount;
    List<FreudFeatureName> featureNames;
    // TYPE NAMES
    int typeNamesCount;
    // SAMPLES
    int samplesCount;
    List<MethodSample> sampleList;

    public MethodDataWrapper(String symbolName) {
        this.symbolNameLen = symbolName.length();
        this.symbolName = symbolName;
    }

    public void serializeForFreudStats() {
        // feature names
        this.featureNamesCount = 1;
        String myVar = "_my_var";
        featureNames = new LinkedList<>();
        featureNames.add(new FreudFeatureName((short) myVar.length(), myVar));

        this.typeNamesCount = 0;


        this.sampleList = new ArrayList<>();


    }


    /* GETTERS & SETTERS */
    // FILE NAME
    public int getSymbolNameLen() {
        return symbolNameLen;
    }

    public String getSymbolName() {
        return symbolName;
    }

    // FEATURE NAMES
    public int getFeatureNamesCount() {
        return featureNamesCount;
    }

    public List<FreudFeatureName> getFeatureNamesList() {
        return featureNames;
    }

    // TYPE NAMES
    public int getTypeNamesCount() {
        return typeNamesCount;
    }

    // SAMPLES
    public int getSamplesCount() {
        return samplesCount;
    }

    public List<MethodSample> getSampleList() {
        return sampleList;
    }

}

