package serializer;

public class FreudFeatureVal {
    private long nameOffset;  // must be same as
    private long typeOffset;
    private long value;

    public FreudFeatureVal(long typeOffset, long value) {
        this.typeOffset = typeOffset;
        this.value = value;
    }

    public void setNameOffset(long nameOffset) {
        this.nameOffset = nameOffset;
    }


    public long getTypeOffset() {
        return typeOffset;
    }

    public long getValue() {
        return value;
    }
}