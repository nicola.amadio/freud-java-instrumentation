package serializer;

public class FreudFeature {

    private String name;
    private int val;

    public FreudFeature(String name, int val) {
        this.name = name;
        this.val = val;
    }

    public FreudFeature() {
        this.name = "";
        this.val = 0;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setVal(int val) {
        this.val = val;
    }

    public String getName() {
        return name;
    }

    public int getVal() {
        return val;
    }
}
