package instrumenter;

import serializer.FreudFeature;
import serializer.FreudFeatureName;
import serializer.FreudFeatureVal;
import serializer.MethodSample;

import java.util.ArrayList;
import java.util.List;

public class FreudLogger {
    private static long startTime, executionTime;
    private static List<FreudFeatureName> featureNames;
    private static List<FreudFeatureVal> featureVals;
    private static FreudFeature tempFeature;
    private static String methodName;

    public static void pushNewEntry(String methodName) {
        FreudLogger.methodName = methodName;
        featureVals = new ArrayList<>();
        featureNames = new ArrayList<>();
    }

    public static synchronized void pushNewFeature( ) {
        tempFeature = new FreudFeature();
    }

    public static synchronized void addFeatureName(String name) {
        tempFeature.setName(name);
    }

    public static synchronized void addFeatureVal(int val) {
        tempFeature.setVal(val);
    }

    public static synchronized void addFeature() {
        featureNames.add(new FreudFeatureName((short) tempFeature.getName().length(), tempFeature.getName()));
        // typeOffset is a system feature, here I set it to 0 but has to be looked into
        featureVals.add(new FreudFeatureVal(0, tempFeature.getVal()));
    }

    public static synchronized void startTiming() {
        startTime = System.currentTimeMillis();
    }

    public static synchronized void measureExecutionTime() {
        executionTime = System.currentTimeMillis() - startTime;
    }

    public static void commitMethodLog() {
        MethodSample sample = new MethodSample(features, executionTime);


    }

}
