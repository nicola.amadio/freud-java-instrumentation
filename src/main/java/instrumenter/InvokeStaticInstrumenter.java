package instrumenter;

import soot.*;
import soot.jimple.*;
import soot.options.Options;
import soot.util.*;

import java.io.File;
import java.util.*;

public class InvokeStaticInstrumenter extends BodyTransformer {

    static SootClass freudLoggerClass;
    static SootMethod startTiming, pushNewEntry, measureExecutionTime, pushNewFeature, addFeatureName,
            addFeatureVal, addFeature, commitMethodLog;
    static {
        String sourceDirectory = System.getProperty("user.dir") +
                File.separator + "target" + File.separator + "classes";
        Options.v().set_soot_classpath(sourceDirectory);
        freudLoggerClass = Scene.v().loadClassAndSupport("instrumenter.FreudLogger");
        startTiming = freudLoggerClass.getMethod("void startTiming()");
        pushNewEntry = freudLoggerClass.getMethod("void pushNewEntry(java.lang.String)");
        pushNewFeature = freudLoggerClass.getMethod("void pushNewFeature()");
        addFeatureName = freudLoggerClass.getMethod("void addFeatureName(java.lang.String)");
        addFeatureVal = freudLoggerClass.getMethod("void addFeatureVal(int)");
        addFeature = freudLoggerClass.getMethod("void addFeature()");
        measureExecutionTime = freudLoggerClass.getMethod("void measureExecutionTime()");
        commitMethodLog = freudLoggerClass.getMethod("void commitMethodLog()");

    }
    final String targetMethod;

    public InvokeStaticInstrumenter(String targetMethod) {
        this.targetMethod = targetMethod;
    }

    /* internalTransform goes through a method body and inserts */
    protected void internalTransform(Body body, String phase, Map options) {
        Stmt newStmt, firstNonIdentityStmt;
        Chain units;
        SootMethod method = body.getMethod();
        if (method.getName().equals(targetMethod)) {
            System.out.println("instrumenting method : " + method.getSignature());
            units = body.getUnits();
            firstNonIdentityStmt = ((JimpleBody) body).getFirstNonIdentityStmt();

            newStmt = createStmtForPushNewEntry(pushNewEntry, method.getName());
            units.insertBefore(newStmt, firstNonIdentityStmt);
            newStmt = createStmtFromSootMethod(startTiming);
            units.insertBefore(newStmt, firstNonIdentityStmt);
            for (Local local: getMethodFeatures(body)) {
                newStmt = createStmtFromSootMethod(pushNewFeature);
                units.insertBefore(newStmt, firstNonIdentityStmt);
                newStmt = createStmtForAddFeatureName(addFeatureName, local.getName());
                units.insertBefore(newStmt, firstNonIdentityStmt);
                newStmt = createStmtForAddFeatureVal(addFeatureVal, local);
                units.insertBefore(newStmt, firstNonIdentityStmt);
                newStmt = createStmtFromSootMethod(addFeature);
                units.insertBefore(newStmt, firstNonIdentityStmt);
            }

            Stmt stmtInLoop;
            Iterator stmtIt = units.snapshotIterator();
            while (stmtIt.hasNext()) {
                stmtInLoop = (Stmt) stmtIt.next();
                if ((stmtInLoop instanceof ReturnStmt) || (stmtInLoop instanceof ReturnVoidStmt)) {
                    newStmt = createStmtFromSootMethod(measureExecutionTime);
                    units.insertBefore(newStmt, stmtInLoop);
                    newStmt = createStmtFromSootMethod(commitMethodLog);
                    units.insertBefore(newStmt, stmtInLoop);
                }
            }
        }
    }

    private Stmt createStmtForPushNewEntry(SootMethod pushNewEntry, String name) {
        InvokeExpr expr = Jimple.v().newStaticInvokeExpr(pushNewEntry.makeRef(), StringConstant.v(name));
        Stmt stmt = Jimple.v().newInvokeStmt(expr);
        return stmt;
    }

    private Stmt createStmtForAddFeatureName(SootMethod addFeatureName, String name) {
        InvokeExpr expr = Jimple.v().newStaticInvokeExpr(addFeatureName.makeRef(), StringConstant.v(name));
        Stmt stmt = Jimple.v().newInvokeStmt(expr);
        return stmt;
    }

    private Stmt createStmtForAddFeatureVal(SootMethod addFeatureVal, Local val) {
        InvokeExpr expr = Jimple.v().newStaticInvokeExpr(InvokeStaticInstrumenter.addFeatureVal.makeRef(), val);
        Stmt stmt = Jimple.v().newInvokeStmt(expr);
        return stmt;
    }

    private Stmt createStmtFromSootMethod(SootMethod method) {
        InvokeExpr expr = Jimple.v().newStaticInvokeExpr(method.makeRef());
        Stmt stmt = Jimple.v().newInvokeStmt(expr);
        return stmt;
    }

    private List<Local> getMethodFeatures(Body body) {
        List<Local> features = new ArrayList<>();
        for (Local l : body.getParameterLocals()) {
            if (l.getType() instanceof IntType) {
                features.add(l);
            }
        }
        return features;
    }

}