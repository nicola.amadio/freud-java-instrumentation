All the commands mentioned here should be run from the project root folder.

Build:
- Add the path to the `rt.jar` on your machine in the `instrumenter.InstrumentMethod.java` class by initializing the `pathToRtJar` variable to it;
- `mvn package -Dmaven.test.skip=true`; (if you run just `mvn package` it won't work because the test suite needs the application .jar, which you are generating here);

Instrument:
- `java -cp target/freud-java-instrumentation-1.0-SNAPSHOT-jar-with-dependencies.jar:target/classes:target/test-classes instrumenter.InstrumentMethod demo.classes.MicroBenchmark testLinear`;
  
Run Instrumented Class (and see the results in the console):
- `cp target/classes/instrumenter/FreudLogger.class sootOutput/instrumenter/`;
- `cp target/classes/instrumenter/FreudFeature.class sootOutput/instrumenter/`;
- `java -cp sootOutput demo.classes.MicroBenchmark 10`.

Here we are instrumenting method `testLinear` for class `demo.classes.MicroBenchmark`, and running it with input value `10`.

Change those if you want to instrument and run something else. 

If you want to instrument an external application (and not a class contained in demo.classes.MicroBenchmark), adapt the class-paths accordingly in the above commands.



